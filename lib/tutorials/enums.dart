void main() {
  Car myCar = Car(carStyle: CartType.SUV);
  print(myCar);
}

class Car {
  CartType carStyle;

  Car({this.carStyle});
}

enum CartType { hatchback, SUV, convertable, coude }
