import 'dart:math';

class CalulatorBrain {
  CalulatorBrain({this.height, this.weight});

  final int height;
  final int weight;

  double _bmi;

  String calculateBMI() {
    _bmi = weight / pow(height / 100, 2);
    return _bmi.toStringAsFixed(1);
  }

  String getResult() {
    if (_bmi >= 25) {
      return 'OVERWEIGHT';
    } else if (_bmi > 18.5) {
      return 'NORNAL';
    } else {
      return 'UNDERWEIGHT';
    }
  }

  String getInterpretation() {
    if (_bmi >= 25) {
      return 'You\'ve a heigher than normal body weight. Try to excercise more!';
    } else if (_bmi > 18.5) {
      return 'You\'ve normal body weight. Good job!';
    } else {
      return 'You\'ve a lower than normal body weight. Try to eat a bit more!';
    }
  }
}
